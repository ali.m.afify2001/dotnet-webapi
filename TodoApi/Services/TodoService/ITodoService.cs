using MongoDB.Bson;
using TodoApi.Models;

namespace TodoApi.Services;

public interface ITodoService
{
    IEnumerable<TodoItem> GetAllTodoItems();
    TodoItem GetTodoItemById(ObjectId id);
    void AddTodoItem(TodoItem newTodoItem);
    void UpdateTodoItem(TodoItem updatedTodoItem);
    void DeleteTodoItemById(ObjectId id);
    void MarkTodoItemAsDone(ObjectId id);
    void MarkTodoItemAsNotDone(ObjectId id);
}