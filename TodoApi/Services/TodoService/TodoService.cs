﻿using MongoDB.Bson;
using TodoApi.Models;
using TodoApi.Data;
using Microsoft.EntityFrameworkCore;
using TodoApi.Exceptions;

namespace TodoApi.Services;

public class TodoService : ITodoService
{
	private readonly TodoItemsMongoDBContext _todoItemsContext;

	public TodoService(TodoItemsMongoDBContext todoItemsContext)
	{
		_todoItemsContext = todoItemsContext;
	}


	public void AddTodoItem(TodoItem newTodoItem)
	{
		try
		{
			_todoItemsContext.TodoItems.Add(newTodoItem);
			_todoItemsContext.SaveChanges();

		}
		catch (Exception ex)
		{

			throw new InvalidOperationException("Failed to add TodoItem", ex);
		}
	}

	public void DeleteTodoItemById(ObjectId id)
	{
		var todoItem = _todoItemsContext.TodoItems.FirstOrDefault(item => item.Id == id);

		if (todoItem != null)
		{
			_todoItemsContext.TodoItems.Remove(todoItem);
			_todoItemsContext.SaveChanges();
		}
		else
		{
			throw new ItemNotFoundException($"Failed to delete. TodoItem with Id: {id} can not be found");
		}
	}

	public void UpdateTodoItem(TodoItem updatedTodoItem)
	{
		var todoItem = _todoItemsContext.TodoItems.FirstOrDefault(item => item.Id == updatedTodoItem.Id);

		if (todoItem != null)
		{
			todoItem.TodoItemName = updatedTodoItem.TodoItemName;
			todoItem.IsComplete = updatedTodoItem.IsComplete;
			todoItem.DueDate = updatedTodoItem.DueDate;
			todoItem.DateCreated = updatedTodoItem.DateCreated;

			_todoItemsContext.TodoItems.Update(todoItem);
			_todoItemsContext.SaveChanges();
		}
		else
		{
			throw new ItemNotFoundException($"{updatedTodoItem}, Failed to update. TodoItem can not be found");
		}
	}

	public IEnumerable<TodoItem> GetAllTodoItems()
	{
		var todoItems = _todoItemsContext.TodoItems.OrderBy(item => item.Id).AsNoTracking().ToList();
		if (todoItems.Count == 0)
		{
			throw new InvalidOperationException("No TodoItems found");
		}
		else
		{
			return todoItems;
		}
	}

	public TodoItem GetTodoItemById(ObjectId id)
	{
		var todoItem = _todoItemsContext.TodoItems.FirstOrDefault(item => item.Id == id);

		if (todoItem != null)
		{
			return todoItem;
		}
		else
		{
			throw new ItemNotFoundException($"TodoItem with Id: {id} not found");
		}
	}

	public void MarkTodoItemAsDone(ObjectId id)
	{
		_setTodoItemIsComplete(id, true);
	}

	public void MarkTodoItemAsNotDone(ObjectId id)
	{
		_setTodoItemIsComplete(id, false);
	}

	private void _setTodoItemIsComplete(ObjectId id, bool state)
	{
		var todoItem = _todoItemsContext.TodoItems.FirstOrDefault(item => item.Id == id);
		if (todoItem != null)
		{

			todoItem.IsComplete = state;
			_todoItemsContext.TodoItems.Update(todoItem);
			_todoItemsContext.SaveChanges();
		}
		else
		{
			throw new ItemNotFoundException("TodoItem can not be found");
		}
	}

}
