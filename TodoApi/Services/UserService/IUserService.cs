using MongoDB.Bson;
using TodoApi.Models;

namespace TodoApi.Services;

public interface IUserService
{
    IEnumerable<User> GetAllUsers();
    User? GetUserById(ObjectId id);
    void AddUser(User newUser);
    void UpdateUser(User updatedUser);
    void DeleteUserById(ObjectId id);
    void AddTodoItem(ObjectId userId, TodoItem newTodoItem);
    void DeleteTodoItemById(ObjectId userId, ObjectId todoItemId);
}