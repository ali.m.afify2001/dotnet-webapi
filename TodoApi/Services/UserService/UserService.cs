using System.Data;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using MongoDB.Driver;
using TodoApi.Data;
using TodoApi.Models;
using TodoApi.Exceptions;

namespace TodoApi.Services;

public class UserService : IUserService
{
    private readonly TodoItemsMongoDBContext _todoItemsContext;
    private readonly ITodoService _todoService;

    public UserService(TodoItemsMongoDBContext todoItemsContext, ITodoService todoService)
    {
        _todoItemsContext = todoItemsContext;
        _todoService = todoService;
    }

    public void AddUser(User newUser)
    {
        try
        {
            _todoItemsContext.Users.Add(newUser);
            _todoItemsContext.SaveChanges();

        }
        catch (Exception ex)
        {
            throw new InvalidOperationException("Failed to add User", ex);
        }
    }

    public void DeleteUserById(ObjectId id)
    {
        var user = _todoItemsContext.Users.FirstOrDefault(user => user.Id == id);

        if (user != null)
        {
            foreach (var todoItemId in user.TodoList)
            {
                _todoService.DeleteTodoItemById(todoItemId);
            }
            _todoItemsContext.Users.Remove(user);
            _todoItemsContext.SaveChanges();
        }
        else
        {
            throw new ItemNotFoundException("Failed to delete. User cannot be found");

        }
    }

    public IEnumerable<User> GetAllUsers()
    {
        var users = _todoItemsContext.Users.OrderBy(user => user.Id).AsNoTracking().ToList();
        if (users.Count == 0)
        {
            throw new InvalidOperationException("No Users found");
        }
        else
        {
            return users;
        }
    }

    public User? GetUserById(ObjectId id)
    {
        var user = _todoItemsContext.Users.FirstOrDefault(user => user.Id == id);

        if (user != null)
        {
            return user;
        }
        else
        {
            throw new ItemNotFoundException("User cannot be found");
        }
    }

    public void UpdateUser(User updatedUser)
    {
        var user = _todoItemsContext.Users.FirstOrDefault(user => user.Id == updatedUser.Id);

        if (user != null)
        {
            user.UserName = updatedUser.UserName;
            user.TodoList = updatedUser.TodoList;

            _todoItemsContext.Users.Update(user);
            _todoItemsContext.SaveChanges();
        }
        else
        {
            throw new ItemNotFoundException("Failed to update. User cannot be found");
        }
    }

    public void AddTodoItem(ObjectId userId, TodoItem newTodoItem)
    {
        _todoService.AddTodoItem(newTodoItem);

        var user = _todoItemsContext.Users.FirstOrDefault(user => user.Id == userId);

        if (user != null)
        {
            user.TodoList.Add(newTodoItem.Id);
            _todoItemsContext.Users.Update(user);
            _todoItemsContext.SaveChanges();
        }
        else
        {
            throw new ItemNotFoundException("Failed to add. User cannot be found");
        }
    }

    public void DeleteTodoItemById(ObjectId userId, ObjectId todoItemId)
    {
        var user = _todoItemsContext.Users.FirstOrDefault(user => user.Id == userId); ;
        if (user != null)
        {
            _todoService.DeleteTodoItemById(todoItemId);

            user.TodoList.Remove(todoItemId);

            _todoItemsContext.Users.Update(user);
            _todoItemsContext.SaveChanges();
        }
        else
        {
            throw new ItemNotFoundException("Failed to delete. User cannot be found");
        }
    }

}