using Microsoft.VisualBasic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.EntityFrameworkCore;

namespace TodoApi.Models;

[Collection("todoItems")]
public class TodoItem
{
    public ObjectId Id { get; set; }

    [BsonElement("name")]
    public string? TodoItemName { get; set; }

    [BsonElement("isComplete")]
    public bool IsComplete { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime DueDate { get; set; }
    public DateTime DueTime { get; set; }
}