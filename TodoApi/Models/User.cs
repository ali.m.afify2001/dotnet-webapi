﻿using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.EntityFrameworkCore;

namespace TodoApi.Models;

[Collection("users")]
public class User
{
    public ObjectId Id { get; set; }

    [BsonElement("name")]
    public string? UserName { get; set; }
 
    [BsonElement("todoList")]
    [Display(Name = "Todo List", Description ="List of TodoItems Ids")]
    public List<ObjectId> TodoList { get; set; } = new List<ObjectId>();
}
