using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;

using TodoApi.Models;
using TodoApi.Services;
using TodoApi.DTOs;

namespace TodoApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class TodoItemsController : ControllerBase
{
    private readonly ITodoService _todoService;

    public TodoItemsController(ITodoService todoService)
    {
        _todoService = todoService;
    }

    // GET: api/TodoItems
    [HttpGet]
    public ActionResult<IEnumerable<TodoItemDTO>> GetTodoItems()
    {
        var todoItemDTOs = new List<TodoItemDTO>();
        var todoItemsDAOs = _todoService.GetAllTodoItems();

        foreach (var todoItemDAO in todoItemsDAOs)
        {

            todoItemDTOs.Add(new TodoItemDTO
            {
                Id = todoItemDAO.Id.ToString(),
                TodoItemName = todoItemDAO.TodoItemName,
                IsComplete = todoItemDAO.IsComplete,
                DueDate = todoItemDAO.DueDate.ToLocalTime().ToShortDateString(),
                DueTime = todoItemDAO.DueTime.ToLocalTime().ToShortTimeString()
            });
        }

        return Ok(todoItemDTOs);
    }

    // GET: api/TodoItems/uuid
    [HttpGet("{id}")]
    public ActionResult<TodoItemDTO> GetTodoItem(string id)
    {
        _ = ObjectId.TryParse(id, out ObjectId todoItemId);

        var todoItemDAO = _todoService.GetTodoItemById(todoItemId);

        var todoItemDTO = new TodoItemDTO
        {
            Id = id,
            TodoItemName = todoItemDAO.TodoItemName,
            IsComplete = todoItemDAO.IsComplete,
            DueDate = todoItemDAO.DueDate.ToLocalTime().ToShortDateString(),
            DueTime = todoItemDAO.DueTime.ToLocalTime().ToShortTimeString()
        };

        return Ok(todoItemDTO);
    }

    // PUT: api/TodoItems/uuid
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPut("{id}")]
    public IActionResult PutTodoItem(string id, TodoItemDTO todoItemDTO)
    {
        _ = ObjectId.TryParse(id, out ObjectId todoItemId);

        var todoItemDAO = _todoService.GetTodoItemById(todoItemId);

        todoItemDAO.TodoItemName = todoItemDTO.TodoItemName ?? todoItemDAO.TodoItemName;
        todoItemDAO.IsComplete = todoItemDTO.IsComplete ?? todoItemDAO.IsComplete;
        todoItemDAO.DueDate = todoItemDTO.DueDate != null ? DateTime.Parse(todoItemDTO.DueDate).ToUniversalTime() : todoItemDAO.DueDate;
        todoItemDAO.DueTime = todoItemDTO.DueTime != null ? DateTime.Parse(todoItemDTO.DueTime).ToUniversalTime() : todoItemDAO.DueTime;

        _todoService.UpdateTodoItem(todoItemDAO);

        return NoContent();
    }

    // POST: api/TodoItems
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPost]
    public ActionResult<TodoItemDTO> PostTodoItem(TodoItemDTO todoItemDTO)
    {
        System.Console.WriteLine("todoItemDTO: " + todoItemDTO.ToJson());

        var todoItemDAO = new TodoItem
        {
            Id = ObjectId.GenerateNewId(),
            TodoItemName = todoItemDTO.TodoItemName,
            IsComplete = todoItemDTO.IsComplete ?? false,
            DueDate = todoItemDTO.DueDate != null ? DateTime.Parse(todoItemDTO.DueDate).ToUniversalTime() : DateTime.MinValue,
            DueTime = todoItemDTO.DueTime != null ? DateTime.Parse(todoItemDTO.DueTime).ToUniversalTime() : DateTime.MinValue,
            DateCreated = DateTime.UtcNow
        };

        _todoService.AddTodoItem(todoItemDAO);


        System.Console.WriteLine("todoItemDAO: " + todoItemDAO.ToJson());


        return CreatedAtAction(nameof(PostTodoItem), new { id = todoItemDAO.Id }, todoItemDTO);
    }

    // DELETE: api/TodoItems/uuid
    [HttpDelete("{id}")]
    public IActionResult DeleteTodoItem(string id)
    {
        ObjectId todoItemId = ObjectId.Parse(id);

        _todoService.DeleteTodoItemById(todoItemId);

        return NoContent();
    }


}
