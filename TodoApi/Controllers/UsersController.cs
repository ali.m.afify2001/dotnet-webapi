using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using TodoApi.Models;
using TodoApi.Services;

namespace TodoApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UsersController : ControllerBase
{
    private readonly IUserService _userService;

    public UsersController(IUserService userService)
    {
        _userService = userService;
    }

    // GET: api/Users
    [HttpGet]
    public ActionResult<IEnumerable<User>> GetUsers()
    {
        return Ok(_userService.GetAllUsers());
    }

    // GET: api/Users/uuid
    [HttpGet("{id}")]
    public ActionResult<TodoItem> GetUser(string id)
    {
        var user = _userService.GetUserById(ObjectId.Parse(id));
        if (user == null)
        {
            return NotFound();
        }

        return Ok(user);
    }

    // PUT: api/Users/uuid
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPut("{id}")]
    public IActionResult PutUser(string id, User user)
    {
        ObjectId userId = ObjectId.Parse(id);
        
        if (userId != user.Id)
        {
            return BadRequest();
        }

        _userService.UpdateUser(user);

        return NoContent();
    }

    // POST: api/Users
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPost]
    public ActionResult<TodoItem> PostUser(User user)
    {
        _userService.AddUser(user);
        return CreatedAtAction(nameof(PostUser), new { id = user.Id }, user);
    }

    // DELETE: api/Users/uuid
    [HttpDelete("{id}")]
    public IActionResult DeleteTodoItem(string id)
    {
        ObjectId userId = ObjectId.Parse(id);
        var user = _userService.GetUserById(userId);
        if (user == null)
        {
            return NotFound();
        }

        _userService.DeleteUserById(userId);
        return NoContent();
    }

   
}
