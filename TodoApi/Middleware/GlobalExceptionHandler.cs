using System.Diagnostics;
using Microsoft.AspNetCore.Diagnostics;
using TodoApi.Exceptions;

namespace TodoApi.Middleware;

public class GlobalExceptionHandler : IExceptionHandler
{
    private readonly ILogger<GlobalExceptionHandler> _logger;

    public GlobalExceptionHandler(ILogger<GlobalExceptionHandler> logger)
    {
        _logger = logger;
    }

    public async ValueTask<bool> TryHandleAsync(HttpContext httpContext, Exception exception, CancellationToken cancellationToken)
    {
        var traceId = Activity.Current?.Id ?? httpContext.TraceIdentifier;

        _logger.LogError(exception, "Unhandled exception. Could not process a request on this machine:{MachineName}. TraceId: {TraceId}",Environment.MachineName, traceId);

        var (statusCode, title) = GetStatusCodeAndMessage(exception);

        await Results.Problem(
            title: title,
            statusCode: statusCode,
            extensions: new Dictionary<string, object?> { { "TraceId", traceId } }
        ).ExecuteAsync(httpContext);

        return true;

    }

    private static (int statusCode, string message) GetStatusCodeAndMessage(Exception exception)
    {
        return exception switch
        {
            ItemNotFoundException => (StatusCodes.Status404NotFound, exception.Message),
            InvalidOperationException => (StatusCodes.Status400BadRequest, exception.Message),
            _ => (StatusCodes.Status500InternalServerError, "Internal Server Error")
        };
    }
}