using Microsoft.EntityFrameworkCore;
using TodoApi.Models;

namespace TodoApi.Data;

public class TodoItemInMemoryContext : DbContext
{
    public TodoItemInMemoryContext(DbContextOptions<TodoItemInMemoryContext> options)
        : base(options)
    {
    }

    public DbSet<TodoItem> TodoItems { get; set; } = null!;
}