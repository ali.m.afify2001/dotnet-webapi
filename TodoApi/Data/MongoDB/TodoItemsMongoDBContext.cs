using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using MongoDB.EntityFrameworkCore.Extensions;
using TodoApi.Models;

namespace TodoApi.Data;


// var connectionString = Environment.GetEnvironmentVariable("MONGODB_URI");

// if (connectionString == null)
// {
//     Console.WriteLine("You must set your 'MONGODB_URI' environment variable. To learn how to set it, see https://www.mongodb.com/docs/drivers/csharp/current/quick-start/#set-your-connection-string");
//     Environment.Exit(0);
// }
// var client = new MongoClient(connectionString);

// var db = MflixDbContext.Create(client.GetDatabase("sample_mflix"));

// var movie = db.Movies.First(m => m.title == "Back to the Future");
// Console.WriteLine(movie.plot);

public class TodoItemsMongoDBContext : DbContext
{
    public DbSet<TodoItem> TodoItems { get; init; }
    public DbSet<User> Users { get; init; }

    // public static TodoItemsMongoDBContext Create(IMongoDatabase database) =>
    //     new(new DbContextOptionsBuilder<TodoItemsMongoDBContext>()
    //         .UseMongoDB(database.Client, database.DatabaseNamespace.DatabaseName)
    //         .Options);

    public TodoItemsMongoDBContext(DbContextOptions options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<TodoItem>();
        modelBuilder.Entity<User>();
    }
}
