namespace TodoApi.Data;

public class MongoDBSettings
{
    public string? ConnectionString { get; set; } = null;
    public string? DatabaseName { get; set; } = null;

    override public string ToString() {
        return $"MongoDBSettings: ConnectionString={ConnectionString}, DatabaseName={DatabaseName}";
    }
}