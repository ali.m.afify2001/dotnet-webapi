using System.Text.Json.Serialization;

namespace TodoApi.DTOs;

public class TodoItemDTO
{
    [JsonPropertyName("id")]
    public string? Id { get; set; }

    [JsonPropertyName("todoItemName")]
    public string? TodoItemName { get; set; }

    [JsonPropertyName("isComplete")]
    public bool? IsComplete { get; set; }

    [JsonPropertyName("dueDate")]
    public string? DueDate { get; set; }

    [JsonPropertyName("dueTime")]
    public string? DueTime { get; set; }
}