﻿namespace TodoApi.Exceptions;

public class ItemNotFoundException : System.Exception
{
    public ItemNotFoundException() { }
    public ItemNotFoundException(string message) : base(message) { }
    public ItemNotFoundException(string message, System.Exception inner) : base(message, inner) { }
}